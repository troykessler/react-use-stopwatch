declare type StopwatchAction = () => void;
declare type StopwatchTime = {
    time: number;
    format: string;
};
declare type StopwatchType = () => [StopwatchTime, StopwatchAction, StopwatchAction, StopwatchAction];
export declare const useStopwatch: StopwatchType;
export {};
