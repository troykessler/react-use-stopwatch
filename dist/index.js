"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const pad = (n) => String(n).padStart(2, "0").substring(0, 2);
exports.useStopwatch = () => {
    const [timerId, setTimerId] = react_1.useState();
    const [time, setTime] = react_1.useState({ time: 0, format: "00:00:00.00" });
    const start = () => {
        setTimerId(setInterval(() => {
            setTime((time) => {
                let remainingTime = time.time + 10;
                let hours = 0;
                let minutes = 0;
                let seconds = 0;
                let milliseconds = 0;
                if (remainingTime / 3600000 >= 1) {
                    hours = Math.floor(remainingTime / 3600000);
                    remainingTime -= hours * 3600000;
                }
                if (remainingTime / 60000 >= 1) {
                    minutes = Math.floor(remainingTime / 60000);
                    remainingTime -= minutes * 60000;
                }
                if (remainingTime / 1000 >= 1) {
                    seconds = Math.floor(remainingTime / 1000);
                    remainingTime -= seconds * 1000;
                }
                milliseconds = remainingTime / 10;
                return {
                    time: time.time + 10,
                    format: `${pad(hours)}:${pad(minutes)}:${pad(seconds)}.${pad(milliseconds)}`
                };
            });
        }, 10));
    };
    const stop = () => {
        clearInterval(timerId);
    };
    const reset = () => {
        clearInterval(timerId);
        setTime({
            time: 0,
            format: "00:00:00.00"
        });
    };
    return [time, start, stop, reset];
};
