# react-use-stopwatch

A React Stopwatch Hook

## Installation

```sh
npm install react-hook-stopwatch
yarn add react-hook-stopwatch
```

## Usage

```javascript
import { useStopwatch } from "react-hook-stopwatch";

export const Stopwatch = () => {
  const [{ time, format }, start, stop, reset] = useStopwatch();

  return (
    <div>
      // Stopwatch Outputs
      <strong>{time}</strong>
      <strong>{format}</strong>
      // Stopwatch Inputs
      <button onClick={() => start()}>Start</button>
      <button onClick={() => stop()}>Stop</button>
      <button onClick={() => reset()}>Reset</button>
    </div>
  );
};
```

```sh
Output: 87660 // Milliseconds
Output: 00:01:27.66 // HH:mm:ss:ms
```
